import os
import re
from copy import deepcopy
from itertools import cycle

import numpy as np
from astropy.io import fits
import h5py

import matplotlib.pyplot as plt

class BatchMaker(object):
  def __init__(self, imgs):
    self.imgs = imgs
    self.generator = cycle(imgs)
    self.idx = 0
    self.total = len(imgs)
    
  def next_batch(self, batch_size):
    samples = [next(self.generator) for i in range(batch_size)]
    
    self.idx += batch_size
    if self.idx >= self.total:
      self.imgs = np.random.permutation(self.imgs)
    
    return np.array(samples)
    

class DataHelper(object):
  def __init__(self, band):
    self.img_dir = os.path.join(os.getenv('HOME'),'Documents/astro_data/orig_images')
    sources = []
    for f in os.listdir(self.img_dir):
      match = re.search('[a-z]+\d_\d+_', f)
      if match and match.group(0)[:-1] not in sources:
        sources.append(match.group(0)[:-1])
    
    self.train_set = sources[:int(len(sources) * .8)]
    self.test_set = sources[-int(len(sources) * .2):]
    self._band = band
    
    print('Test set:{len(self.test_set)} Train set:{len(self.train_set)}')
    
  def make_data(self, k):
    for data in [('test',self.test_set),('train',self.train_set)]:
      x, y = [], []
      print(f'Working on {data[0]}...')
      
      count = 1
      total = len(data[1])
      for i in range(len(data[1])):
        src = data[1][i]
      
        print(f'{count/total}....', end='\r')
        count+=1
        
        img = fits.getdata(os.path.join(self.img_dir, f'GDS_{src}_{self._band}.fits'))
        seg = fits.getdata(os.path.join(self.img_dir, f'GDS_{src}_segmap.fits'))
        _img, _seg = deepcopy(img), deepcopy(seg)
        
        del img
        del seg
        
        noise = self._get_noise(_img,_seg)
        
        _x, _y = self._get_data(noise, k)
        x.extend(_x)
        y.extend(_y)
      
      print('Done processing......')
      
      x = np.array(x)
      y = np.array(y)
      
      print('Opening HDF5')
      f = h5py.File(data[0], 'w')
      
      print('Create dataset x...')
      dset = f.create_dataset('x', x.shape)
      dset[...] = x
      
      print('Create dataset y...')
      dset = f.create_dataset('y', y.shape)
      dset[...] = y

      print('Closing file...')
      f.close()
    

  def _get_noise(self, img, segmap):
    img[segmap!=0] = 0
    return img
  
  def _get_data(self, img, k):
    x, y = [], []
    for i in range(img.shape[0]-k):
      for j in range(img.shape[1]-k):
        sample = img[i:i+k,j:j+k]
        if (sample==0).sum() == 0:
          # the label will be the original image
          y.append(sample[:,:])
          
          # 50% data will have some entries zeroed out
          prob_drop = .1 
          sample[np.random.binomial(1, prob_drop, size=sample.shape)] = 0
          x.append(sample)
          
    return x, y

dh = DataHelper('h')
dh.make_data(10)