#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 25 11:48:45 2017

@author: ryanhausen
"""
import numpy as np
import json

from GaussianProcess import GP, RBF
from DataTools import _nmpy_decode, get_noise, get_sr
from ImageTools import radial_frame, fill_radial_frame, effective_radius
from ImageTools import trans_to_origin, trans_from_origin, scale_image, PIL_tuple
from PIL import Image
from astropy.io import fits

def generate_batch(batch_size):
    # x [batch_size, 84, 84, 1]
    # y [batch_size, 5]

    # disk, spheroid distribution in the measured set - .785
    morph = np.random.binomial(1, .5, batch_size)

    batch_x, batch_y = [], []
    for i in range(batch_size):
        if morph[i]:
            batch_x.append(generate_disk())
            batch_y.append(np.array([1.0, 0.0, 0.0, 0.0, 0.0]))
        else:
            batch_x.append(generate_spheroid())
            batch_y.append(np.array([0.0, 1.0, 0.0, 0.0, 0.0]))

    return np.array(batch_x), np.array(batch_y)

def generate_disk(return_fits=False):
    return _generate_image('disk', return_fits)

def generate_spheroid(return_fits=False):
    return _generate_image('spheroid', return_fits)

def generate_unknown(return_fits=False):
    img = [get_noise(c, (84,84)) for c in ['h', 'j', 'v', 'z']]
    return np.dstack(img)

def _sbp_predict_on(morph, x):
    # restore a pretrained GP and kernel
    disk_params = [4.22574589, 2.82598596]
    sph_params = [4.12495443, 2.74516939]
    params = disk_params if morph=='disk' else sph_params
    kernel = RBF(params)
    gp_sbp = GP(kernel)
    gp_sbp.restore(f'{morph}_gp_model.json')

    return gp_sbp.sample(x)

def _generate_image_from(morph, ar, re, seed=None):
    shape = (84, 84)
    seed = seed if seed else np.random.randint(0, 2**32-1)
    # create a matrix in units in effective radaii
    img = radial_frame(shape[0], shape[1], shape[0] // 2, shape[1] // 2) / re

    # the unique radial values to be predicted on by the gaussian process
    x = np.sort(np.unique(img))
    x = x[x < 10.0]
    x = x[:, np.newaxis]

    np.random.seed(seed)
    sbp = _sbp_predict_on(morph, x)
    sbp = 10**sbp[0, :]
    sbp = {r:v for r, v in zip(x[:,0], sbp)}

    h = fill_radial_frame(img.copy(), sbp)

    # validate and adjust the image if the effective radius doesn't match the
    # desired effective radius
    measured_re = effective_radius(h, np.ones_like(h, dtype=bool))
    if measured_re != re:
        re_ratio = measured_re / re
        img *= re_ratio

        x = np.sort(np.unique(img))
        x = x[x < 10.0]
        x = x[:, np.newaxis]

        np.random.seed(seed)
        sbp = _sbp_predict_on(morph, x)
        sbp = 10**sbp[0, :]
        sbp = {r:v for r, v in zip(x[:,0], sbp)}

        h = fill_radial_frame(img.copy(), sbp)

    # set the axis ratio
    # apply the drawn axis ratio
    a = np.sqrt(re**2 * ar) / re
    b = np.sqrt(re**2 / ar) / re

    trans = trans_to_origin().dot(scale_image(a, b)).dot(trans_from_origin())
    trans = PIL_tuple(trans)
    tmp = Image.fromarray(h)
    tmp = tmp.transform(h.shape, Image.AFFINE, trans, resample=Image.BICUBIC)
    h = np.asarray(tmp)
    h.setflags(write=1)

    return h, x, img

def _get_other_bands(h, x, img_re, morph):
    # get the color profile for the image
    params = [1.0, 3.0]
    kernel = RBF(params)
    gp_color = GP(kernel)
    gp_color.restore(f'{morph}_gp_colors.json')

    color_ratios, _ = gp_color.predict(x)
    j, v, z, = h.copy(), h.copy(), h.copy()

    # apply the color profile to make the image in the other bands
    # apply the axis ratio
    for color, img in [(color_ratios[:, 0], j),
                       (color_ratios[:, 1], v),
                       (color_ratios[:, 2], z)]:
        color = np.exp(color)
        multiplier = {r:v for r, v in zip(x[:,0], color)}
        ratio = fill_radial_frame(img_re.copy(), multiplier)
        img *= ratio

    return j, v, z

def _generate_image(morph, return_fits, axis_ratio=None, effective_radius=None, seed=None):
    header = {'morph':morph}

    # get an ar and re drawn from a gaussian
    with open('ar_re_gaussian.json', 'r') as f:
        ar_re = json.load(f)

    mu, cov = ar_re[morph]['mu'], ar_re[morph]['cov']
    mu, cov = _nmpy_decode(mu), _nmpy_decode(cov)

    ar, re = np.random.multivariate_normal(mu, cov)

    ar = axis_ratio if axis_ratio else ar
    re = effective_radius if effective_radius else re

    header['axisr'] = ar
    header['Re'] = re

    h, x, img_re = _generate_image_from(morph, ar, re, seed)

    j, v, z = _get_other_bands(h, x, img_re, morph)

    # add noise
    # scale noise
    sr_ratio = get_sr(morph)
    for color, img in [('h', h), ('j', j), ('v', v), ('z', z)]:
        header[f'sr-{color}'] = sr_ratio[color]
        noise = get_noise(color, img.shape)
        n_mean = noise.mean()
        i_max = img.max()
        sr = i_max / n_mean
        img *= (sr_ratio[color] /  sr)
        #noise = noise * (sr / sr_ratio[color])
        img += noise

    if return_fits:
        header['dim0'] = 'h,j,v,z'
        hdr = fits.Header()
        for k in header.keys():
            hdr[k] = header[k]
        img = np.dstack((h, j, v, z))

        return fits.PrimaryHDU(header=hdr, data=img)
    else:
        return np.dstack((h, j, v, z))
