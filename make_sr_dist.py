import os
import json

import numpy as np

from DataTools import safe_fits_open
import ImageTools as it

def rms(collection):
    return np.sqrt(len(collection)*(np.square(collection)))

home = os.getenv('HOME')
img_dir = 'Documents/astro_data/orig_images'
img_dir = os.path.join(home, img_dir)

img_data = dict()
sn_data = dict()

# iteratte throgh all images
img_files = os.listdir(img_dir)
total = len(img_files)
count = 1
for img_file in sorted(img_files):
    print(count/total, end='\r')
    count += 1

    if '.fits' in img_file:
        name_split = img_file.replace('.fits', '').split('_')
        img_key = '_'.join(name_split[:-1])
        band_key = name_split[-1]
        raw = safe_fits_open(os.path.join(img_dir, img_file))
        if (raw.shape[0] != 84 or raw.shape[1] != 84):
            continue

        img_data.setdefault(img_key, dict())[band_key] = raw

        # do we have all of the bands and segmap
        if len(img_data[img_key].keys()) == 5:
            src_map = img_data[img_key]['segmap'] == int(img_key.split('_')[2])
            noise_map = img_data[img_key]['segmap'] == 0

            for b in ['h', 'j', 'v', 'z']:
                img = img_data[img_key][b]
                re = it.effective_radius(img, src_map)
                cx, cy = np.where(img==np.max(img[src_map]))
                cx, cy = cx[0], cy[0]
                radial_grid = it.radial_frame(img.shape[0], img.shape[1], cx, cy)

                # signal is the sum of the signal within the effective radius
                s = img[radial_grid <= re].sum()

                # noise is the RMS of the noise vlaues at >= 3*Re
                nmap = np.logical_and(noise_map, (radial_grid >= 3*re))
                n = rms(img[nmap].flatten())

                sn_data.setdefault(img_key, dict())

                sn_data[img_key][b] = str(s/n)

            img_data[img_key] = None

with open('sr_vals.json', 'w') as f:
    json.dump(sn_data, f)